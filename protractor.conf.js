'use strict';

require('babel-core/register');

module.exports.config = {
    allScriptsTimeout: 100000,
    seleniumAddress: 'http://localhost:4444/wd/hub',
    capabilities: {
        browserName: 'chrome',
        chromeOptions: {
            args: ['--disable-gpu', '--no-sandbox', '--disable-extensions', '--disable-dev-shm-usage']
        },
        shardTestFiles: true,
        maxInstances: 2
    },
    params: {
    
    },
    specs: ['Specs/*.spec.js'],
};